Requirements
------------

* Ubuntu 16.04
* PHP 7.1
* Nginx
* MySql
* Rabbit-MQ Server
* Composer (PHP package manager)

Installation
------------

1. Install the requirements on the server
2. Move the application code to web root
3. Run `composer install`to install dependencies
4. Configure Nginx in `/etc/nginx/sites-available/site` with the following configuration:

```ini
server {
    server_name domain.tld www.domain.tld;
    root /var/www/project/public;
    location / {
        try_files $uri /index.php$is_args$args;
    }
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }
    location ~ \.php$ {
        return 404;
    }
    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
```

5. Connect to Rabbit-MQ via SSH tunnel

```bash
ssh root@server-ip -L 15672:localhost:15672 -N
```

visit the address in the browser: `http://localhost:15672`  
login with usr `guest` passw `guest`  
select `Exchange` tab and add an item named `task`   
do the same in `Queue` tab  

6. Run the Rabbit-MQ consumers on server boot

```ini
/etc/supervisor/conf.d/task_consumer.conf

[program:task_consumer]
command=/srv/crypto/bin/console rabbitmq:consumer task -m 10 -l 64
autostart=true
autorestart=true
stderr_logfile=/var/log/task.err.log
stdout_logfile=/var/log/task.out.log
numprocs=4
process_name=%(program_name)s_%(process_num)02d
```

7. Restart supervisor service

```bash
systemctl restart supervisor.service
```

8. Create a database in mysql name `crypto`. Migrate the database from dump.sql file.

9. Visit the route on the server domain.com/exchange to generate the initial exchange data