<?php declare(strict_types=1);

namespace App\Common\Bus;

trait BusAwareTrait
{
    /** @var BusInterface */
    private $bus;

    /**
     * @return BusInterface
     */
    protected function getBus(): BusInterface
    {
        return $this->bus;
    }

    /**
     * @param BusInterface $bus
     */
    public function setBus(BusInterface $bus): void
    {
        $this->bus = $bus;
    }
}
