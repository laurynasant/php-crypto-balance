<?php declare(strict_types=1);

namespace App\Common\Bus;

interface BusInterface
{
    /**
     * Executes the given command and optionally returns a value
     *
     * @param object $command
     *
     * @return mixed
     */
    public function handle($command);
}
