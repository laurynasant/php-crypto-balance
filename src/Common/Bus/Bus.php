<?php declare(strict_types=1);

namespace App\Common\Bus;

use League\Tactician\CommandBus;

class Bus implements BusInterface
{
    /** @var CommandBus */
    private $bus;

    /**
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @inheritDoc
     */
    public function handle($command)
    {
        return $this->bus->handle($command);
    }
}
