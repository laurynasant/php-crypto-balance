<?php declare(strict_types=1);

namespace App\Service;

use App\Enum\Currency;

interface ClientInterface
{
    /**
     * @param Currency $currency
     *
     * @return float
     */
    public function getBalance(Currency $currency): float;

    /**
     * @return string
     */
    public function getExchangeName(): string;

    /**
     * @return array
     */
    public function getBalanceList(): array;
}
