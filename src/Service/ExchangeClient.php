<?php declare(strict_types=1);

namespace App\Service;

use App\Command\HelperTrait;
use Symfony\Component\Console\Output\OutputInterface;

class ExchangeClient
{
    use HelperTrait;

    /** @var ClientInterface[] */
    private $exchanges = [];

    /** @var OutputInterface */
    private $output;

    /**
     * @return array
     */
    public function getBalanceList(): array
    {
        $balances = [];

        foreach ($this->exchanges as $exchange) {
            $exchangeName = $exchange->getExchangeName();
            $this->writeln($exchangeName, 'Requested balance');

            try {
                foreach ($exchange->getBalanceList() as $currency => $balance) {
                    $balances[$exchangeName][$currency] = $balance;
                }

                $this->writeln($exchangeName, 'Received balance');
            } catch (\Throwable $throwable) {
                $this->writeln($exchangeName, sprintf('Exception: <error>%s</error>', $throwable->getMessage()));
            }
        }

        return $balances;
    }

    /**
     * @param ClientInterface $exchange
     */
    public function addExchange(ClientInterface $exchange): void
    {
        $this->exchanges[$exchange->getExchangeName()] = $exchange;
    }

    /**
     * @return ClientInterface[]
     */
    public function getExchanges(): array
    {
        return $this->exchanges;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    /**
     * @inheritDoc
     */
    protected function getOutput(): OutputInterface
    {
        return $this->output;
    }
}
