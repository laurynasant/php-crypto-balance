<?php declare(strict_types=1);

namespace App\Service;

use App\Enum\Currency;
use GuzzleHttp\Client;
use App\Command\HelperTrait;
use Symfony\Component\Console\Output\OutputInterface;

class ExchangeRateClient
{
    use HelperTrait;

    /** @var array */
    private $rates;

    /** @var OutputInterface */
    private $output;

    /**
     * @param Currency $currency
     * @param float $amount
     *
     * @return float
     */
    public function calculateRate(Currency $currency, float $amount): float
    {
        $rate = $this->getExchangeRates()[$currency->getName()] ?? 0;

        return (float)($rate * $amount);
    }

    /**
     * @return array
     */
    public function getExchangeRates(): array
    {
        if ($this->rates !== null) {
            return $this->rates;
        }

        foreach ($this->fetchRates() as $rate) {
            $this->rates[$rate['symbol']] = (float)($rate['price_usd'] ?? 0);
        }

        return $this->rates;
    }

    /**
     * @return array
     */
    private function fetchRates(): array
    {
        $this->output !== null && $this->writeln('CoinMarketCap', 'Requested exchange rates');
        $response = (new Client())->get('https://api.coinmarketcap.com/v1/ticker/');
        $this->output !== null && $this->writeln('CoinMarketCap', 'Received exchange rates');

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    /**
     * @inheritDoc
     */
    protected function getOutput(): OutputInterface
    {
        return $this->output;
    }
}
