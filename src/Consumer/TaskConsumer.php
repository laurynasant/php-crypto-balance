<?php declare(strict_types=1);

namespace App\Consumer;

use App\Utility\Utils;
use App\Common\Bus\BusInterface;
use App\Common\Bus\BusAwareTrait;
use PhpAmqpLib\Message\AMQPMessage;
use App\UseCase\FetchFromExchange\FetchFromExchange;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;

class TaskConsumer implements ConsumerInterface
{
    use BusAwareTrait;

    /**
     * @param BusInterface $bus
     */
    public function __construct(BusInterface $bus)
    {
        $this->setBus($bus);
    }

    /**
     * @inheritDoc
     */
    public function execute(AMQPMessage $msg)
    {
        $msg = Utils::json($msg->getBody())->decode();

        if ( ! isset($msg['taskId'])) {
            return true;
        }

        try {
            $this->getBus()->handle(new FetchFromExchange($msg['taskId']));
        } catch (\Throwable $throwable) {
            // ignore
        }

        usleep(200000);

        return true;
    }
}
