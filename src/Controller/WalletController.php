<?php declare(strict_types=1);

namespace App\Controller;

use App\Model\TaskModel;
use App\Common\Bus\BusInterface;
use App\Common\Bus\BusAwareTrait;
use App\UseCase\QueryLogs\QueryLogs;
use App\Storage\TaskStorageInterface;
use App\UseCase\QueryTasks\QueryTasks;
use App\UseCase\QueryWallet\QueryWallet;
use App\UseCase\CreateSession\CreateSession;
use App\Storage\ExchangeRateStorageInterface;
use Symfony\Component\HttpFoundation\Response;
use App\UseCase\CreateExchangeRate\CreateExchangeRate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route(service="app.controller.wallet")
 */
class WalletController extends AbstractController
{
    use BusAwareTrait;

    /** @var array */
    private $banks = ['Deutsche Bank', 'Nordea', 'SEB', 'Swedbank', 'Luminor'];

    /** @var array */
    private $banksForLogs = ['Deutsche Bank', 'Nordea', 'SEB', 'Swedbank', 'Luminor'];

    /** @var array */
    private $timesForBankLogs = [109, 345, 243, 503, 153, 414, 685, 856, 329, 378];

    /** @var TaskStorageInterface */
    private $taskStorage;

    /** @var ExchangeRateStorageInterface */
    private $exchangeRateStorage;

    /**
     * @param BusInterface $bus
     * @param TaskStorageInterface $taskStorage
     * @param ExchangeRateStorageInterface $rateStorage
     */
    public function __construct(
        BusInterface $bus,
        TaskStorageInterface $taskStorage,
        ExchangeRateStorageInterface $rateStorage
    ) {

        $this->setBus($bus);
        $this->taskStorage = $taskStorage;
        $this->exchangeRateStorage = $rateStorage;
    }

    /**
     * @Route("/api/{sessionId}", name="app_api")
     *
     * @param string $sessionId
     *
     * @return Response
     */
    public function apiAction(string $sessionId): Response
    {
        /** @var TaskModel[] $tasks */
        $tasks = $this->getBus()->handle(new QueryTasks($sessionId));

        $results = [];
        foreach ($tasks as $id => $task) {
            $query = new QueryWallet($task->getExchangeName(), $task->getSessionId());
            $wallet = $this->getBus()->handle($query);

            $totalBtcBalance = 0;
            $totalUsdBalance = 0;

            foreach ($wallet as $item) {
                if ($item['currency'] === 'BTC') {
                    $totalBtcBalance = $item['balance'] ?? 0;
                }

                $totalUsdBalance += $item['value']['USD'] ?? 0;
            }

            $results[] = [
                'exchangeName' => $task->getExchangeName(),
                'type' => 'crypto',
                'estimatedValue' => [
                    'USD' => sprintf("%0.2f", $totalUsdBalance),
                    'BTC' => sprintf("%0.4f", $totalBtcBalance),
                ],
                'wallet' => $wallet,
            ];

            if (in_array($id, [1, 3, 5, 7, 9])) {
                $results[] = $this->generateBankData();
            }
        }

        $logs = [];
        foreach ($this->getBus()->handle(new QueryLogs($sessionId)) as $id => $log) {
            $logs[] = $log;

            if (in_array($id, [3, 7, 11, 15, 19])) {
                $bank = array_shift($this->banksForLogs);

                $logs[] = $this->addBankLog($bank, 'Requested Authorization Grant');
                $logs[] = $this->addBankLog(
                    $bank,
                    '<span class="text-success">Received Access Token</span>',
                    array_shift($this->timesForBankLogs)
                );

                $logs[] = $this->addBankLog($bank, 'Requested Asset Balance');
                $logs[] = $this->addBankLog(
                    $bank,
                    '<span class="text-success">Received Asset Balance</span>',
                    array_shift($this->timesForBankLogs)
                );
            }
        }

        $response = [
            'meta' => [
                'status' => $this->taskStorage->hasActiveTasks($sessionId) ? 'progress' : 'ok',
            ],
            'data' => $results,
            'logs' => $logs,
        ];

        return $this->json($response, 200, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @param string $bankName
     * @param string $text
     * @param int|null $time
     *
     * @return string
     */
    private function addBankLog(string $bankName, string $text, int $time = null): string
    {
        $message = sprintf('<span class="text-color-blue">%s</span> > %s', $bankName, $text);
        $message .= $time === null ? '' : sprintf(' (0.%s)', $time);

        return $message;
    }

    /**
     * @return array
     */
    private function generateBankData(): array
    {
        $rates = $this->exchangeRateStorage->getExchangeRates();
        $bank = array_shift($this->banks);
        $balances = [];
        $accountCount = in_array($bank, ['SEB', 'Deutsche Bank']) ? 2 : 1;

        for ($i = 0; $i < $accountCount; $i++) {
            $btc = $this->generateFakeBalance();
            $eur = $btc * (float)($rates['BTC'] ?? 0);

            $balances[] = [
                'btc' => $btc,
                'eur' => $eur,
            ];
        }

        return [
            'exchangeName' => $bank,
            'type' => 'fiat',
            'estimatedValue' => [
                'EUR' => sprintf("%0.2f", array_sum(array_column($balances, 'eur'))),
                'BTC' => sprintf("%0.4f", array_sum(array_column($balances, 'btc'))),
            ],
            'wallet' => $this->generateBankWallet($balances),
        ];
    }

    /**
     * @param array $balances
     *
     * @return array
     */
    private function generateBankWallet(array $balances): array
    {
        $wallets = [];

        foreach ($balances as $balance) {
            $wallets[] = [
                'currency' => 'EUR',
                'balance' => sprintf("%0.2f", $balance['eur']),
                'value' => [
                    'BTC' => sprintf("%0.8f", $balance['btc']),
                ],
            ];
        }

        return $wallets;
    }

    /**
     * @return float
     */
    private function generateFakeBalance(): float
    {
        return (rand(100, 10000000) / 1000000) / 4;
    }

    /**
     * @Route("/", name="app_wallet")
     */
    public function walletAction(): Response
    {
        return $this->render('main.html.twig');
    }

    /**
     * @Route("/exchange", name="app_fetch_exchange_rate")
     */
    public function fetchExchangeRateAction(): Response
    {
        $this->getBus()->handle(new CreateExchangeRate());

        return $this->json(['ok']);
    }

    /**
     * @Route("/reload", name="app_reload")
     */
    public function reloadAction(): Response
    {
        $command = new CreateSession();
        $this->getBus()->handle($command);

        return $this->json(['id' => $command->getId()], 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
