<?php declare(strict_types=1);

namespace App\Enum;

use MabeEnum\Enum;

/**
 * @method static Currency BTC()
 * @method static Currency ETH()
 * @method static Currency LTC()
 * @method static Currency ETC()
 * @method static Currency XRP()
 * @method static Currency DASH()
 * @method static Currency NEO()
 */
class Currency extends Enum
{
    const BTC = 'btc';
    const ETH = 'eth';
    const LTC = 'ltc';
    const ETC = 'etc';
    const XRP = 'xrp';
    const DASH = 'dash';
    const NEO = 'neo';
}
