<?php declare(strict_types=1);

namespace App\Utility;

use App\Utility\Utils\IdUtility;
use App\Utility\Utils\JsonUtility;
use App\Utility\Utils\DateUtility;

class Utils
{
    /**
     * @param string|array $input
     *
     * @return JsonUtility
     */
    public static function json($input): JsonUtility
    {
        return new JsonUtility($input);
    }

    /**
     * @param \DateTime|string|null $date
     *
     * @return DateUtility
     */
    public static function date($date = null): DateUtility
    {
        if (is_string($date)) {
            return DateUtility::fromString($date);
        } elseif ($date instanceof \DateTime) {
            return new DateUtility($date);
        }

        return new DateUtility();
    }

    /**
     * @return IdUtility
     */
    public static function id(): IdUtility
    {
        return new IdUtility();
    }
}
