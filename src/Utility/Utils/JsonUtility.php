<?php declare(strict_types=1);

namespace App\Utility\Utils;

class JsonUtility
{
    /** @var string|array */
    private $input;

    /**
     * @param $input
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * @return array
     */
    public function decode(): array
    {
        $response = json_decode($this->input, true);

        return is_array($response) ? $response : [$response];
    }

    /**
     * @param int $options
     *
     * @return string
     */
    public function encode(int $options = JSON_UNESCAPED_UNICODE): string
    {
        return json_encode($this->input, $options);
    }
}
