<?php declare(strict_types=1);

namespace App\Utility\Utils;

use Ramsey\Uuid\Uuid;

class IdUtility
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return Uuid::uuid1()->toString();
    }
}
