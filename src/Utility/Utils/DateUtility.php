<?php declare(strict_types=1);

namespace App\Utility\Utils;

class DateUtility
{
    /** @var \DateTime */
    private $dateTime;

    /**
     * @param \DateTime $dateTime
     */
    public function __construct(\DateTime $dateTime = null)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param string $date
     *
     * @return DateUtility
     */
    public static function fromString(string $date): self
    {
        return new self(new \DateTime($date));
    }

    /**
     * @param \DateTime $dateTime
     */
    public function setDateTime(\DateTime $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time): void
    {
        $this->setDateTime(new \DateTime($time));
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime ?? new \DateTime();
    }
}
