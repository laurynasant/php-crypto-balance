<?php declare(strict_types=1);

namespace App\Model;

class ExchangeModel
{
    /** @var string */
    private $exchangeName;

    /** @var string */
    private $currency;

    /** @var float */
    private $balance;

    /**
     * @param string $exchangeName
     * @param string $currency
     * @param float $balance
     */
    public function __construct(string $exchangeName, string $currency, float $balance)
    {
        $this->exchangeName = $exchangeName;
        $this->currency = $currency;
        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getExchangeName(): string
    {
        return $this->exchangeName;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }
}
