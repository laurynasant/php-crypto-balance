<?php declare(strict_types=1);

namespace App\Model;

class TaskModel
{
    /** @var string */
    private $taskId;

    /** @var string */
    private $sessionId;

    /** @var string */
    private $exchangeName;

    /** @var string */
    private $status;

    /**
     * @param string $taskId
     * @param string $sessionId
     * @param string $exchangeName
     * @param string $status
     */
    public function __construct(string $taskId, string $sessionId, string $exchangeName, string $status)
    {
        $this->taskId = $taskId;
        $this->sessionId = $sessionId;
        $this->exchangeName = $exchangeName;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTaskId(): string
    {
        return $this->taskId;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getExchangeName(): string
    {
        return $this->exchangeName;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
}
