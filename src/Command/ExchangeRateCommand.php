<?php declare(strict_types=1);

namespace App\Command;

use App\Service\ExchangeRateClient;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExchangeRateCommand extends Command
{
    /** @var ExchangeRateClient */
    private $client;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('app:rate');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->client->setOutput($output);
        $rates = $this->client->getExchangeRates();

        $this->renderTable($output, $rates);
    }

    /**
     * @param OutputInterface $output
     * @param array $rates
     */
    private function renderTable(OutputInterface $output, array $rates)
    {
        $table = new Table($output);
        $table->setHeaders(['Currency', 'Price USD']);

        foreach ($rates as $currency => $price) {
            $table->addRow([$currency, sprintf("%0.2f", $price)]);
        }

        $output->writeln('');
        $table->render();
    }

    /**
     * @param ExchangeRateClient $client
     */
    public function setClient(ExchangeRateClient $client): void
    {
        $this->client = $client;
    }
}
