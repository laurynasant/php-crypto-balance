<?php declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Output\OutputInterface;

trait HelperTrait
{
    /**
     * @return OutputInterface
     */
    abstract protected function getOutput(): OutputInterface;

    /**
     * @param string $title
     * @param string $text
     */
    private function writeln(string $title, string $text): void
    {
        $this->getOutput()->writeln(sprintf(
            '<info>[%s]</info> <comment>%s</comment> > %s',
            (new \DateTime())->format('H:i:s.u'),
            $title,
            $text
        ));
    }
}
