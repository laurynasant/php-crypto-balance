<?php declare(strict_types=1);

namespace App\Command;

use App\Enum\Currency;
use App\Service\ExchangeClient;
use App\Service\ExchangeRateClient;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BalanceCommand extends Command
{
    /** @var ExchangeClient */
    private $exchangeClient;

    /** @var ExchangeRateClient */
    private $rateClient;

    /** @var bool */
    private $fake = false;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('app:balance');
        $this->addOption('fake', null, InputOption::VALUE_NONE);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->fake = $input->getOption('fake');
        $this->rateClient->setOutput($output);
        $this->exchangeClient->setOutput($output);

        $balances = $this->exchangeClient->getBalanceList();

        $this->renderTable($output, $balances);
    }

    /**
     * @param OutputInterface $output
     * @param array $balances
     */
    private function renderTable(OutputInterface $output, array $balances)
    {
        $table = new Table($output);
        $table->setHeaders(['Exchange', 'Currency', 'Balance', 'Balance USD']);

        foreach ($balances as $exchange => $balanceList) {
            foreach ($balanceList as $currency => $balance) {
                if ($this->fake === true) {
                    $balance = $this->generateFakeBalance();
                }

                $currency = Currency::byName($currency);

                $table->addRow([
                    $exchange,
                    $currency->getName(),
                    sprintf("%0.8f", $balance),
                    sprintf("%0.2f", $this->rateClient->calculateRate($currency, $balance)),
                ]);
            }
        }

        $output->writeln('');
        $table->render();
    }

    /**
     * @return float
     */
    private function generateFakeBalance(): float
    {
        return (rand(100, 99999999) / 1000000) / 2;
    }

    /**
     * @param ExchangeClient $exchangeClient
     */
    public function setExchangeClient(ExchangeClient $exchangeClient): void
    {
        $this->exchangeClient = $exchangeClient;
    }

    /**
     * @param ExchangeRateClient $rateClient
     */
    public function setRateClient(ExchangeRateClient $rateClient): void
    {
        $this->rateClient = $rateClient;
    }
}
