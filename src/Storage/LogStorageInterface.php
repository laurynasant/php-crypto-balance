<?php declare(strict_types=1);

namespace App\Storage;

interface LogStorageInterface
{
    /**
     * @param string $logId
     * @param string $sessionId
     * @param string $message
     */
    public function create(string $logId, string $sessionId, string $message): void;

    /**
     * @param string $sessionId
     *
     * @return array
     */
    public function getLogsBySessionId(string $sessionId): array;
}
