<?php declare(strict_types=1);

namespace App\Storage;

class LogStorage extends AbstractStorage implements LogStorageInterface
{
    /**
     * @inheritDoc
     */
    public function create(string $logId, string $sessionId, string $message): void
    {
        $sql = $this->twig->render('sql/log/create.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('log_id', $logId);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->bindValue('message', $message);
        $stmt->bindValue('created_at', new \DateTime(), 'datetime');
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            throw new \Exception('Log not created');
        }
    }

    /**
     * @inheritDoc
     */
    public function getLogsBySessionId(string $sessionId): array
    {
        $sql = $this->twig->render('sql/log/get-all-by-session-id.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
