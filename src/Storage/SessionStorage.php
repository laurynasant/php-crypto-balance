<?php declare(strict_types=1);

namespace App\Storage;

class SessionStorage extends AbstractStorage implements SessionStorageInterface
{
    /**
     * @inheritDoc
     */
    public function create(string $sessionId): void
    {
        $sql = $this->twig->render('sql/session/create.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->bindValue('status', 'progress');
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            throw new \Exception('Not created');
        }
    }
}
