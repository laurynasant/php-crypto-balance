<?php declare(strict_types=1);

namespace App\Storage;

use App\Model\ExchangeModel;

interface WalletStorageInterface
{
    /**
     * @param string $entryId
     * @param string $sessionId
     * @param ExchangeModel $exchange
     */
    public function create(string $entryId, string $sessionId, ExchangeModel $exchange): void;

    /**
     * @param string $sessionId
     * @param $exchangeName
     *
     * @return array
     */
    public function getBySessionIdAndExchange(string $sessionId, $exchangeName): array;
}
