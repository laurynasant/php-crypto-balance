<?php declare(strict_types=1);

namespace App\Storage;

use App\Model\TaskModel;

interface TaskStorageInterface
{
    /**
     * @param string $taskId
     * @param string $sessionId
     * @param string $exchange
     */
    public function create(string $taskId, string $sessionId, string $exchange): void;

    /**
     * @param string $taskId
     * @param string $status
     */
    public function updateStatus(string $taskId, string $status): void;

    /**
     * @param string $taskId
     *
     * @return TaskModel
     */
    public function get(string $taskId): TaskModel;

    /**
     * @param string $sessionId
     *
     * @return array|TaskModel[]
     */
    public function getFinishedTasksBySessionId(string $sessionId): array;

    /**
     * @param string $sessionId
     *
     * @return bool
     */
    public function hasActiveTasks(string $sessionId): bool;
}
