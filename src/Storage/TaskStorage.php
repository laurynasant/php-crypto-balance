<?php declare(strict_types=1);

namespace App\Storage;

use App\Factory\TaskFactory;
use App\Model\TaskModel;

class TaskStorage extends AbstractStorage implements TaskStorageInterface
{
    /**
     * @inheritDoc
     */
    public function create(string $taskId, string $sessionId, string $exchange): void
    {
        $sql = $this->twig->render('sql/task/create.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('task_id', $taskId);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->bindValue('exchange', $exchange);
        $stmt->bindValue('status', 'todo');
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            throw new \Exception('Task not created');
        }
    }

    /**
     * @inheritDoc
     */
    public function updateStatus(string $taskId, string $status): void
    {
        $sql = $this->twig->render('sql/task/update-status.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('task_id', $taskId);
        $stmt->bindValue('status', $status);
        $stmt->execute();
    }

    /**
     * @inheritDoc
     */
    public function get(string $taskId): TaskModel
    {
        $sql = $this->twig->render('sql/task/all-by-task-id.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('task_id', $taskId);
        $stmt->execute();

        return TaskFactory::create($stmt->fetch());
    }

    /**
     * @inheritDoc
     */
    public function getFinishedTasksBySessionId(string $sessionId): array
    {
        $sql = $this->twig->render('sql/task/get-all-finished-by-session-id.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->execute();

        $results = [];
        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $task) {
            $results[] = TaskFactory::create($task);
        }

        return $results;
    }

    /**
     * @inheritDoc
     */
    public function hasActiveTasks(string $sessionId): bool
    {
        $sql = $this->twig->render('sql/task/all-active-by-session-id.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->execute();

        return count($stmt->fetchAll(\PDO::FETCH_ASSOC)) > 0;
    }
}
