<?php declare(strict_types=1);

namespace App\Storage;

use App\Model\ExchangeModel;

class WalletStorage extends AbstractStorage implements WalletStorageInterface
{
    /**
     * @inheritDoc
     */
    public function create(string $entryId, string $sessionId, ExchangeModel $exchange): void
    {
        $sql = $this->twig->render('sql/wallet/create.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('entry_id', $entryId);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->bindValue('exchange', $exchange->getExchangeName());
        $stmt->bindValue('currency', $exchange->getCurrency());
        $stmt->bindValue('balance', $exchange->getBalance());
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            throw new \Exception('Wallet entry not created');
        }
    }

    /**
     * @inheritDoc
     */
    public function getBySessionIdAndExchange(string $sessionId, $exchangeName): array
    {
        $sql = $this->twig->render('sql/wallet/get-all-by-session-id-and-exchange-name.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('session_id', $sessionId);
        $stmt->bindValue('exchange', $exchangeName);
        $stmt->execute();

        $results = [];
        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $entry) {
            $results[] = [
                'currency' => $entry['currency'],
                'balance' => (float)$entry['balance'],
            ];
        }

        return $results;
    }
}
