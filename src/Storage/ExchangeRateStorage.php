<?php declare(strict_types=1);

namespace App\Storage;

class ExchangeRateStorage extends AbstractStorage implements ExchangeRateStorageInterface
{
    /** @var array */
    private $rates;

    /**
     * @inheritDoc
     */
    public function create(string $rateId, string $currencyName, float $price): void
    {
        $sql = $this->twig->render('sql/exchange-rate/create.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('rate_id', $rateId);
        $stmt->bindValue('currency', $currencyName);
        $stmt->bindValue('price', $price);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            throw new \Exception('Exchange rate not created');
        }
    }

    /**
     * @inheritDoc
     */
    public function getByCurrency(string $currency): float
    {
        $sql = $this->twig->render('sql/exchange-rate/get-one-by-currency-name.sql.twig');
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('currency', $currency);
        $stmt->execute();

        return (float)$stmt->fetchColumn();
    }

    /**
     * @inheritDoc
     */
    public function getExchangeRates(): array
    {
        if ($this->rates === null) {
            $sql = $this->twig->render('sql/exchange-rate/get-all.sql.twig');
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $this->rates = [];

            foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $entry) {
                $this->rates[$entry['currency']] = $entry['price'];
            }
        }

        return $this->rates;
    }
}
