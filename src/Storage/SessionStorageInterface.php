<?php declare(strict_types=1);

namespace App\Storage;

interface SessionStorageInterface
{
    /**
     * @param string $sessionId
     */
    public function create(string $sessionId): void;
}
