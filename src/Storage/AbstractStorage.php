<?php declare(strict_types=1);

namespace App\Storage;

use Doctrine\DBAL\Connection;
use Twig_Environment as Twig;

abstract class AbstractStorage
{
    /** @var Twig */
    protected $twig;

    /** @var Connection */
    protected $connection;

    /**
     * @param Twig $twig
     * @param Connection $connection
     */
    public function __construct(Twig $twig, Connection $connection)
    {
        $this->twig = $twig;
        $this->connection = $connection;
    }
}
