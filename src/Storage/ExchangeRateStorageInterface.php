<?php declare(strict_types=1);

namespace App\Storage;

interface ExchangeRateStorageInterface
{
    /**
     * @param string $rateId
     * @param string $currencyName
     * @param float $price
     */
    public function create(string $rateId, string $currencyName, float $price): void;

    /**
     * @param string $currency
     *
     * @return float
     */
    public function getByCurrency(string $currency): float;

    /**
     * @return array
     */
    public function getExchangeRates(): array;
}
