<?php declare(strict_types=1);

namespace App\Factory;

use App\Model\TaskModel;

class TaskFactory
{
    /**
     * @param array $data
     *
     * @return TaskModel
     * @throws \Exception
     */
    public static function create(array $data): TaskModel
    {
        $self = new self();

        return new TaskModel(
            $self->get($data, 'task_id'),
            $self->get($data, 'session_id'),
            $self->get($data, 'exchange'),
            $self->get($data, 'status')
        );
    }

    /**
     * @param array $data
     * @param string $key
     *
     * @return mixed
     * @throws \Exception
     */
    private function get(array $data, string $key)
    {
        if ( ! isset($data[$key])) {
            throw new \Exception(sprintf('Field "%s" not found', $key));
        }

        return $data[$key];
    }
}
