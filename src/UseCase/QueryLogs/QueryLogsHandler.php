<?php declare(strict_types=1);

namespace App\UseCase\QueryLogs;

use App\Storage\LogStorageInterface;

class QueryLogsHandler
{
    /** @var LogStorageInterface */
    private $logStorage;

    /**
     * @param LogStorageInterface $logStorage
     */
    public function __construct(LogStorageInterface $logStorage)
    {
        $this->logStorage = $logStorage;
    }

    /**
     * @param QueryLogs $query
     *
     * @return array
     */
    public function handle(QueryLogs $query): array
    {
        $results = [];

        foreach ($this->logStorage->getLogsBySessionId($query->getSessionId()) as $log) {
            $results[] = $log['message'];
        }

        return $results;
    }
}
