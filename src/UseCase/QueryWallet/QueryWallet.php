<?php declare(strict_types=1);

namespace App\UseCase\QueryWallet;

class QueryWallet
{
    /** @var string */
    private $exchangeName;

    /** @var string */
    private $sessionId;

    /**
     * @param string $exchangeName
     * @param string $sessionId
     */
    public function __construct(string $exchangeName, string $sessionId)
    {
        $this->exchangeName = $exchangeName;
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getExchangeName(): string
    {
        return $this->exchangeName;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
