<?php declare(strict_types=1);

namespace App\UseCase\QueryWallet;

use App\Storage\ExchangeRateStorageInterface;
use App\Storage\WalletStorageInterface;

class QueryWalletHandler
{
    /** @var WalletStorageInterface */
    private $walletStorage;

    /** @var ExchangeRateStorageInterface */
    private $exchangeRateStorage;

    /**
     * @param WalletStorageInterface $walletStorage
     * @param ExchangeRateStorageInterface $rateStorage
     */
    public function __construct(WalletStorageInterface $walletStorage, ExchangeRateStorageInterface $rateStorage)
    {
        $this->walletStorage = $walletStorage;
        $this->exchangeRateStorage = $rateStorage;
    }

    /**
     * @param QueryWallet $query
     *
     * @return array
     */
    public function handle(QueryWallet $query): array
    {
        $results = $this->walletStorage->getBySessionIdAndExchange($query->getSessionId(), $query->getExchangeName());

        foreach ($results as $key => $result) {
            $results[$key]['balance'] = sprintf("%0.8f", $result['balance']);
            $results[$key]['value'] = [
                'USD' => $this->calculateRate($result['currency'], (float)$results[$key]['balance']),
            ];
        }

        return $results;
    }

    /**
     * @param string $currency
     * @param float $amount
     *
     * @return string
     */
    private function calculateRate(string $currency, float $amount): string
    {
        $rate = $this->getRates()[$currency] ?? 0;

        return sprintf("%0.2f", (float)($rate * $amount));
    }

    /**
     * @return array
     */
    private function getRates(): array
    {
        return $this->exchangeRateStorage->getExchangeRates();
    }
}
