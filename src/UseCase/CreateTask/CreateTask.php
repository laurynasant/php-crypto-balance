<?php declare(strict_types=1);

namespace App\UseCase\CreateTask;

class CreateTask
{
    /** @var string */
    private $sessionId;

    /**
     * @param string $sessionId
     */
    public function __construct(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
