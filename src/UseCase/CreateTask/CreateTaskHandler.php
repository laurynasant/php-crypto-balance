<?php declare(strict_types=1);

namespace App\UseCase\CreateTask;

use App\Utility\Utils;
use App\Service\ExchangeClient;
use App\Storage\TaskStorageInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class CreateTaskHandler
{
    /** @var ExchangeClient */
    private $exchangeClient;

    /** @var TaskStorageInterface */
    private $taskStorage;

    /** @var ProducerInterface */
    private $producer;

    /**
     * @param ExchangeClient $exchangeClient
     * @param TaskStorageInterface $taskStorage
     * @param ProducerInterface $producer
     */
    public function __construct(
        ExchangeClient $exchangeClient,
        TaskStorageInterface $taskStorage,
        ProducerInterface $producer
    ) {
        $this->exchangeClient = $exchangeClient;
        $this->taskStorage = $taskStorage;
        $this->producer = $producer;
    }

    /**
     * @param CreateTask $command
     */
    public function handle(CreateTask $command): void
    {
        foreach ($this->exchangeClient->getExchanges() as $exchange) {
            $taskId = $this->createTaskId();

            $this->taskStorage->create($taskId, $command->getSessionId(), $exchange->getExchangeName());
            $this->produceMessage($taskId);
        }
    }

    /**
     * @return string
     */
    private function createTaskId(): string
    {
        return Utils::id()->generate();
    }

    /**
     * @param string $taskId
     */
    private function produceMessage(string $taskId): void
    {
        $this->producer->publish(Utils::json(['taskId' => $taskId])->encode());
    }
}
