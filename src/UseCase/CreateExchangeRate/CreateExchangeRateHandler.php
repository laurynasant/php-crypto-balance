<?php declare(strict_types=1);

namespace App\UseCase\CreateExchangeRate;

use App\Service\ExchangeRateClient;
use App\Storage\ExchangeRateStorageInterface;
use App\Utility\Utils;

class CreateExchangeRateHandler
{
    /** @var ExchangeRateClient */
    private $exchangeRateClient;

    /** @var ExchangeRateStorageInterface */
    private $exchangeRateStorage;

    /**
     * @param ExchangeRateClient $exchangeRateClient
     * @param ExchangeRateStorageInterface $rateStorage
     */
    public function __construct(ExchangeRateClient $exchangeRateClient, ExchangeRateStorageInterface $rateStorage)
    {
        $this->exchangeRateClient = $exchangeRateClient;
        $this->exchangeRateStorage = $rateStorage;
    }

    /**
     * @param CreateExchangeRate $command
     */
    public function handle(CreateExchangeRate $command): void
    {
        foreach ($this->exchangeRateClient->getExchangeRates() as $currency => $amount) {
            $this->exchangeRateStorage->create(Utils::id()->generate(), $currency, $amount);
        }
    }
}
