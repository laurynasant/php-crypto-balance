<?php declare(strict_types=1);

namespace App\UseCase\CreateSession;

use App\Utility\Utils;

class CreateSession
{
    /** @var string */
    private $id;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->id = Utils::id()->generate();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
