<?php declare(strict_types=1);

namespace App\UseCase\CreateSession;

use App\Common\Bus\BusInterface;
use App\Common\Bus\BusAwareTrait;
use App\UseCase\CreateTask\CreateTask;
use App\Storage\SessionStorageInterface;

class CreateSessionHandler
{
    use BusAwareTrait;

    /** @var SessionStorageInterface */
    private $sessionStorage;

    /**
     * @param SessionStorageInterface $sessionStorage
     * @param BusInterface $bus
     */
    public function __construct(SessionStorageInterface $sessionStorage, BusInterface $bus)
    {
        $this->sessionStorage = $sessionStorage;
        $this->setBus($bus);
    }

    /**
     * @param CreateSession $command
     */
    public function handle(CreateSession $command): void
    {
        $sessionId = $command->getId();
        $this->sessionStorage->create($sessionId);

        $this->getBus()->handle(new CreateTask($sessionId));
    }
}
