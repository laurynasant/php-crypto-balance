<?php declare(strict_types=1);

namespace App\UseCase\QueryTasks;

use App\Model\TaskModel;
use App\Storage\TaskStorageInterface;

class QueryTasksHandler
{
    /** @var TaskStorageInterface */
    private $taskStorage;

    /**
     * @param TaskStorageInterface $taskStorage
     */
    public function __construct(TaskStorageInterface $taskStorage)
    {
        $this->taskStorage = $taskStorage;
    }

    /**
     * @param QueryTasks $query
     *
     * @return array|TaskModel[]
     */
    public function handle(QueryTasks $query): array
    {
        $sessionId = $query->getSessionId();

        return $this->taskStorage->getFinishedTasksBySessionId($sessionId);
    }
}
