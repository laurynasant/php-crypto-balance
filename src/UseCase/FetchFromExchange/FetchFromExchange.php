<?php declare(strict_types=1);

namespace App\UseCase\FetchFromExchange;

class FetchFromExchange
{
    /** @var string */
    private $taskId;

    /**
     * @param string $taskId
     */
    public function __construct(string $taskId)
    {
        $this->taskId = $taskId;
    }

    /**
     * @return string
     */
    public function getTaskId(): string
    {
        return $this->taskId;
    }
}
