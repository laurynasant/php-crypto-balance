<?php declare(strict_types=1);

namespace App\UseCase\FetchFromExchange;

use App\Utility\Utils;
use App\Model\ExchangeModel;
use App\Service\ExchangeClient;
use App\Service\ClientInterface;
use App\Storage\LogStorageInterface;
use App\Storage\TaskStorageInterface;
use App\Storage\WalletStorageInterface;

class FetchFromExchangeHandler
{
    /** @var TaskStorageInterface */
    private $taskStorage;

    /** @var WalletStorageInterface */
    private $walletStorage;

    /** @var LogStorageInterface */
    private $logStorage;

    /** @var ExchangeClient */
    private $exchangeClient;

    /**
     * @param TaskStorageInterface $taskStorage
     * @param WalletStorageInterface $walletStorage
     * @param LogStorageInterface $logStorage
     * @param ExchangeClient $exchangeClient
     */
    public function __construct(
        TaskStorageInterface $taskStorage,
        WalletStorageInterface $walletStorage,
        LogStorageInterface $logStorage,
        ExchangeClient $exchangeClient
    ) {
        $this->taskStorage = $taskStorage;
        $this->walletStorage = $walletStorage;
        $this->logStorage = $logStorage;
        $this->exchangeClient = $exchangeClient;
    }

    /**
     * @param FetchFromExchange $command
     */
    public function handle(FetchFromExchange $command): void
    {
        $taskId = $command->getTaskId();

        $startTime = microtime(true);
        $throwable = null;

        $task = $this->taskStorage->get($taskId);
        $this->taskStorage->updateStatus($task->getTaskId(), 'progress');
        $this->createLogMessage($task->getSessionId(), $task->getExchangeName(), 'Requested balance');

        try {
            $exchangeClient = $this->getExchangeClientByName($task->getExchangeName());
            foreach ($exchangeClient->getBalanceList() as $currency => $balance) {

                $balance = $this->generateFakeBalance();

                $entryId = Utils::id()->generate();
                $exchangeModel = new ExchangeModel($task->getExchangeName(), $currency, $balance);
                $this->walletStorage->create($entryId, $task->getSessionId(), $exchangeModel);
            }

            $this->taskStorage->updateStatus($taskId, 'done');
            $message = '<span class="text-success">Received balance</span>';
        } catch (\Throwable $throwable) {
            $this->taskStorage->updateStatus($taskId, 'failed');
            $message = sprintf('<span class="text-error">Exception:</span> %s', $throwable->getMessage());
        }

        $diff = microtime(true) - $startTime;
        $sec = intval($diff);
        $micro = $diff - $sec;

        $this->createLogMessage(
            $task->getSessionId(),
            $task->getExchangeName(),
            sprintf('%s (%ss)', $message, sprintf("%0.3f", $micro))
        );
    }

    /**
     * @return float
     */
    private function generateFakeBalance(): float
    {
        return (rand(100, 10000000) / 1000000) / 2;
    }

    /**
     * @param string $sessionId
     * @param string $exchange
     * @param string $message
     */
    private function createLogMessage(string $sessionId, string $exchange, string $message): void
    {
        $message = sprintf('%s > %s', $exchange, $message);
        $this->logStorage->create(Utils::id()->generate(), $sessionId, $message);
    }

    /**
     * @param string $exchangeName
     *
     * @return ClientInterface
     * @throws \Exception
     */
    private function getExchangeClientByName(string $exchangeName): ClientInterface
    {
        $exchanges = $this->exchangeClient->getExchanges();

        if ( ! isset($exchanges[$exchangeName])) {
            throw new \Exception(sprintf('Exchange client with name "%s" not found', $exchangeName));
        }

        return $exchanges[$exchangeName];
    }
}
