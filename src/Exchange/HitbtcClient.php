<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\Exchange;
use ccxt\hitbtc;

class HitbtcClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new hitbtc($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'HitBTC';
    }
}
