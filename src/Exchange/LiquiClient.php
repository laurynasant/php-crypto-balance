<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\liqui;
use ccxt\Exchange;

class LiquiClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new liqui($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'Liqui';
    }
}
