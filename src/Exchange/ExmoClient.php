<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\exmo;
use ccxt\Exchange;

class ExmoClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new exmo($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'EXMO';
    }
}
