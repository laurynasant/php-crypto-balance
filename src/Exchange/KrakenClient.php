<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\kraken;
use ccxt\Exchange;

class KrakenClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new kraken($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'Kraken';
    }
}
