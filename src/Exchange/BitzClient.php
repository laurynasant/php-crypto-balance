<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\bitz;
use ccxt\Exchange;

class BitzClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new bitz($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'Bit-Z';
    }
}
