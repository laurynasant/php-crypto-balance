<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\wex;
use ccxt\Exchange;

class WexClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new wex($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'WEX';
    }
}
