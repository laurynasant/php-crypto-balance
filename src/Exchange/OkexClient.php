<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\okex;
use ccxt\Exchange;

class OkexClient extends AbstractClient
{
    /**
     * @inheritdoc
     */
    protected function getClient(): Exchange
    {
        return new okex($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'OKEx';
    }
}
