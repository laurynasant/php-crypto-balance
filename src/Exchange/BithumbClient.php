<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\bithumb;
use ccxt\Exchange;

class BithumbClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new bithumb($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'Bithumb';
    }
}
