<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\coinegg;
use ccxt\Exchange;
use App\Enum\Currency;
use ccxt\ExchangeError;

class CoineggClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new coinegg($this->getConfig());
    }

    /**
     * @inheritDoc
     */
    public function getBalance(Currency $currency): float
    {
        try {
            return parent::getBalance($currency);
        } catch (ExchangeError $error) {
            if ($error->getMessage() === 'coineggLack of balance') {
                return 0;
            }

            throw $error;
        }
    }

    /**
     * @inheritDoc
     */
    public function getExchangeName(): string
    {
        return 'CoinEgg';
    }
}
