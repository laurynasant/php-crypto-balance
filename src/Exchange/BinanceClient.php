<?php declare(strict_types=1);

namespace App\Exchange;

use ccxt\binance;
use ccxt\Exchange;

class BinanceClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    protected function getClient(): Exchange
    {
        return new binance($this->getConfig());
    }

    /**
     * @inheritdoc
     */
    public function getExchangeName(): string
    {
        return 'Binance';
    }
}
