(function() {
    var vm = new Vue({

        el: '#app',

        delimiters: ['${', '}'],

        data: {
            logs: null,
            exchanges: null,
            showExchanges: false,
            getBtnDisabled: false,
            showBtnDisabled: true,
            showLogs: true,
            sessionId: null
        },

        methods: {
            getLogs: function() {
                this.getSessionId();
                this.getBtnDisabled = true;
                this.showBtnDisabled = true;
            },

            showData: function() {
                vm.showLogs = false;
                vm.showExchanges = true;
            },

            getData: function(sessionId) {
                this.$http.get('/api/' + sessionId).then(function (response) {

                    if ( response.body.meta.status == 'ok') {
                        this.exchanges = response.body.data;
                        this.showBtnDisabled = false;
                        this.getBtnDisabled = false;
                    }

                    if ( response.body.meta.status == 'progress') {
                        this.logs = response.body.logs;

                        setTimeout(function() {
                            vm.getData(vm.sessionId);
                        }, 1000);
                    }
                }, function() {
                    setTimeout(function() {
                        this.getBtnDisabled = false;
                        vm.getData(vm.sessionId);
                    }, 1000);
                });
            },

            getSessionId: function() {
                this.$http.get('/reload').then(function (response) {

                    vm.sessionId = response.body.id;

                    vm.getData(response.body.id);

                });
            },

            scrollToEnd: function() {
                var container = this.$el.querySelector("#logs");
                container.scrollTop = container.scrollHeight;
            }
        }
    });
})();